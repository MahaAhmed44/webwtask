  <br><br>
  <footer id="footer" class="bg-light">
    

    <div class="container">
      <div class="copyright text-primary">
        &copy; Copyright <strong class="text-primary">NewBiz</strong>. All Rights Reserved
      </div>
      <div class="credits text-primary">
        Designed by <a href="https://bootstrapmade.com/" class="text-primary">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="includes/lib/jquery/jquery.min.js"></script>
  <script src="includes/lib/jquery/jquery-migrate.min.js"></script>
  <script src="includes/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="includes/lib/easing/easing.min.js"></script>
  <script src="includes/lib/mobile-nav/mobile-nav.js"></script>
  <script src="includes/lib/wow/wow.min.js"></script>
  <script src="includes/lib/waypoints/waypoints.min.js"></script>
  <script src="includes/lib/counterup/counterup.min.js"></script>
  <script src="includes/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="includes/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="includes/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="includes/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
