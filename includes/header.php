<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Quiz System</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="includes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="includes/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="includes/lib/animate/animate.min.css" rel="stylesheet">
  <link href="includes/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="includes/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="includes/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="includes/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="includes/img/logo11.png" alt="" class="img-fluid"></a>
       
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li class="drop-down"><a href="">Login</a>
            <ul>
              <li><a href="index.php">User</a></li>
              <li><a href="admin.php">Admin</a></li>
            </ul>
          </li>
          <li><a href="registerform.php">Register</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
       <div class="header">
          <h2 class="text-light">Login</h2>
        </div>
        <form method="post" action="login.php">
         <div class="form-group">
            <label  class="text-light">Username</label>
            <input type="text" name="username" class="form-control"   placeholder="Username" required>
         </div>
    
         <div class="form-group">
            <label  class="text-light">Password</label>
            <input type="password" name="password"  class="form-control" placeholder="Password" required>
         </div>
         
          <div class="form-group">
            <button type="submit" name="login" class="btn btn-primary">Login</button>
         </div>
         <p class="text-light">
           Not yet a member? <a href="registerform.php" class="text-danger">Sign up</a>
         </p>
       
          </form>
       
      </div>

      <div class="intro-info">
        <h2>We provide<br><span>solutions</span><br>for your tests!</h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->