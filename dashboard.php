<?php
 session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Quiz System</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="includes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="includes/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="includes/lib/animate/animate.min.css" rel="stylesheet">
  <link href="includes/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="includes/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="includes/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="includes/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
 <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="includes/img/logo11.png" alt="" class="img-fluid"></a>
       
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="quizhome.php">Select Exam</a></li>
          <li class="active"><a href="#">Last Results</a></li>
          <li class="drop-down"><a href=""><?php echo $_SESSION["username"]; ?></a>
            <ul>
              <li><a href="logout.php">Logout</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->
  <section id="intro" class="clearfix">
    <div class="container">

      
       <div class="intro-info">
        <h2>We provide<br><span>solutions</span><br>for your tests!</h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
        </div>
      </div>

      <div class="intro-info">
        <h2>Welcome</h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Start Quiz</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->
  <!-- content -->
 <section id="about">
  <div class="row">
   
 </div>
      <div class="row mt">
      <div class="col-md-3">
      </div>
          <div class="col-lg-6">
             <div id="countdowntimer" style="display:block;"></div>
             <br><br>
            <form action="quiz.php" method="post">
              <table class="table">
                <tbody>
                <?php
                  $db = mysqli_connect('localhost','root','','webwtask');
                  
                  $id=$_GET["id"];
                  $res=mysqli_query($db,"select * from questions where id=$id") or die( mysqli_error($db));
                  while($row=mysqli_fetch_array($res))
                  {
                     
                   ?>
                    <tr>
                    <td><?php echo $row["question"]; ?></td>
                   </tr>
                    <tr>
                    <td><input type="radio" name="opt1"><?php echo $row["opt1"]; ?></td>
                   </tr>
                    <tr>
                    <td><input type="radio" name="opt2"><?php echo $row["opt2"]; ?></td>
                   </tr>
                    <tr>
                    <td><input type="radio" name="opt3"><?php echo $row["opt3"]; ?></td>
                   </tr>
                    <tr>
                    <td><input type="radio" name="opt4"><?php echo $row["opt4"]; ?></td>
                   </tr>
                   <?php
                  }
                ?>
                 
                
                </tbody>
              </table>
               <button class="btn btn-primary" type="submit" name="submit">Submit</button>
           </form>
          </div>
          <!-- /col-lg-12 -->
          <div class="col-md-3">
          </div>
        </div>
        <!-- /row -->
  </section> 
  <!-- end of content -->
  <br><br>
  <footer id="footer" class="bg-light">
    

    <div class="container">
      <div class="copyright text-primary">
        &copy; Copyright <strong class="text-primary">NewBiz</strong>. All Rights Reserved
      </div>
      <div class="credits text-primary">
        Designed by <a href="https://bootstrapmade.com/" class="text-primary">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script type="text/javascript">
    setInterval(function()
    {
      timer();
    },1000);
    function timer()
    {
      var xmlhttp=new XMLHttpRequest();
      xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
         if (xmlhttp.responseText == "00:00:01")
         {
          window.location="result.php";
         }
         document.getElementById("countdowntimer").innerHTML=xmlhttp.responseText;
        }
      };
      xmlhttp.open("GET","load_timer.php",true);
      xmlhttp.send(null);
    }
  </script>
  <script src="includes/lib/jquery/jquery.min.js"></script>
  <script src="includes/lib/jquery/jquery-migrate.min.js"></script>
  <script src="includes/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="includes/lib/easing/easing.min.js"></script>
  <script src="includes/lib/mobile-nav/mobile-nav.js"></script>
  <script src="includes/lib/wow/wow.min.js"></script>
  <script src="includes/lib/waypoints/waypoints.min.js"></script>
  <script src="includes/lib/counterup/counterup.min.js"></script>
  <script src="includes/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="includes/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="includes/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="includes/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
