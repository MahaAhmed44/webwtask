<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Quiz System</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="includes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="includes/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="includes/lib/animate/animate.min.css" rel="stylesheet">
  <link href="includes/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="includes/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="includes/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="includes/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="#intro" class="scrollto"><img src="includes/img/logo11.png" alt="" class="img-fluid"></a>
       
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li class="drop-down"><a href="">Login</a>
            <ul>
              <li><a href="index.php">User</a></li>
              <li><a href="#">Admin</a></li>
            </ul>
          </li>
          <li><a href="registerform.php">Register</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->
  <section id="intro" class="clearfix">
    <div class="container">

      <div class="intro-img">
        <!--img src="includes/img/intro-img.svg" alt="" class="img-fluid"-->
        <div class="header">
          <h2 class="text-light">Register</h2>
        </div>
          <form method="post" action="register.php">
          
         <div class="form-group">
            <label  class="text-light">Username</label>
            <input type="text" name="username" class="form-control"   placeholder="Username" required>
         </div>
         <div class="form-group">
            <label  class="text-light">Email</label>
            <input type="email" name="email" class="form-control"  placeholder="Email" required>
         </div>
         <div class="form-group">
            <label  class="text-light">Password</label>
            <input type="password" name="password_1"  class="form-control" placeholder="Password" required>
         </div>
         <div class="form-group">
            <label  class="text-light">Confirm Password</label>
            <input type="password" name="password_2"  class="form-control" placeholder="Password" required>
         </div>
          <div class="form-group">
            <button type="submit" name="register" class="btn btn-primary">Register</button>
         </div>
         <p class="text-light">
           Already a member? <a href="index.php" class="text-danger">Sign in</a>
         </p>
       
          </form>
      </div>

      <div class="intro-info">
        <h2>We provide<br><span>solutions</span><br>for your tests!</h2>
        <div>
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
        </div>
      </div>

    </div>
  </section><!-- #intro -->

<br><br>
  <footer id="footer" class="bg-light">
    

    <div class="container">
      <div class="copyright text-primary">
        &copy; Copyright <strong class="text-primary">NewBiz</strong>. All Rights Reserved
      </div>
      <div class="credits text-primary">
        Designed by <a href="https://bootstrapmade.com/" class="text-primary">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="includes/lib/jquery/jquery.min.js"></script>
  <script src="includes/lib/jquery/jquery-migrate.min.js"></script>
  <script src="includes/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="includes/lib/easing/easing.min.js"></script>
  <script src="includes/lib/mobile-nav/mobile-nav.js"></script>
  <script src="includes/lib/wow/wow.min.js"></script>
  <script src="includes/lib/waypoints/waypoints.min.js"></script>
  <script src="includes/lib/counterup/counterup.min.js"></script>
  <script src="includes/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="includes/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="includes/lib/lightbox/js/lightbox.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="includes/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>

