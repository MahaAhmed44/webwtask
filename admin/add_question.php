<?php 
  $db = mysqli_connect('localhost','root','','webwtask');
  $id=$_GET["id"];
  $category='';
  $res=mysqli_query($db,"select * from exam_category where id=$id");
  while($row=mysqli_fetch_array($res))
  {
   $category=$row["category"];
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Quiz System</title>

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><b>DASH<span>IO</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
          <!-- settings start -->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-tasks"></i>
              <span class="badge bg-theme">4</span>
              </a>
          </li>
          <!-- settings end -->
          <!-- inbox dropdown start-->
          <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-envelope-o"></i>
              <span class="badge bg-theme">5</span>
              </a>
          </li>
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->
          <li id="header_notification_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
              <i class="fa fa-bell-o"></i>
              <span class="badge bg-warning">7</span>
              </a>
          </li>
          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
      <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
         
          <li class="mt">
            <a class="active" href="adminhome.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-book"></i>
              <span>Exams</span>
              </a>
            <ul class="sub">
              <li><a href="add_exam.php">Add Exam</a></li>
              <li><a href="show_exam.php">Show Exams</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Questions</span>
              </a>
            <ul class="sub">
             <li><a href="select_category.php">Add Question</a></li>
              <li><a href="show_question.php">Show Questions</a></li>
            </ul>
          </li>
         
      </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i>Add Question Inside <?php echo "<font color='red'>".$category."</font>"; ?></h3>
       <div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i>Add New Question</h4>
            <div class="form-panel">
              <form name="form1" action="" method="post" role="form" class="form-horizontal style-form">
                <div class="form-group has-success">
                  <label class="col-lg-2 control-label">Question</label>
                  <div class="col-lg-10">
                    <input type="text" placeholder="add question" name="question"   class="form-control">
                   
                  </div>
                </div>

                 <div class="form-group has-success">
                  <label class="col-lg-2 control-label">Add Opt1</label>
                  <div class="col-lg-10">
                    <input type="text" placeholder="add opt1" name="opt1"   class="form-control">
                   
                  </div>
                </div>

                 <div class="form-group has-success">
                  <label class="col-lg-2 control-label">Add Opt2</label>
                  <div class="col-lg-10">
                    <input type="text" placeholder="add opt2" name="opt2"   class="form-control">
                   
                  </div>
                </div>

                 <div class="form-group has-success">
                  <label class="col-lg-2 control-label">Add Opt3</label>
                  <div class="col-lg-10">
                    <input type="text" placeholder="add opt3" name="opt3"   class="form-control">
                   
                  </div>
                </div>

                 <div class="form-group has-success">
                  <label class="col-lg-2 control-label">Add Opt4</label>
                  <div class="col-lg-10">
                    <input type="text" placeholder="add opt4" name="opt4"   class="form-control">
                   
                  </div>
                </div>

                 <div class="form-group has-success">
                  <label class="col-lg-2 control-label">Add Answer</label>
                  <div class="col-lg-10">
                    <input type="text" placeholder="add answer" name="answer"   class="form-control">
                   
                  </div>
                </div>
                
                
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <button class="btn btn-theme" type="submit" name="submit1">Add Question</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
       </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
   
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="lib/form-validation-script.js"></script>

</body>

</html>
<?php 
 
if(isset($_POST['submit1'])){
  $loop=0;
   $count=0;
   $res=mysqli_query($db,"select * from questions where category='$category' order by id asc") or die(mysqli_error($db));
   $count=mysqli_num_rows($res);
   if($count==0)
   {

   }
   else
   {
    while($row=mysqli_fetch_array($res))
    {
     $loop=$loop+1;
     mysqli_query($db,"update questions set question_no='$loop' where id=$row[id]");
    }
   }
   $loop=$loop+1;
   mysqli_query($db,"insert into questions values(NULL,'$loop','$_POST[question]','$_POST[opt1]','$_POST[opt2]','$_POST[opt3]','$_POST[opt4]','$_POST[answer]','$category')") or die(mysqli_error($db));
}
?>