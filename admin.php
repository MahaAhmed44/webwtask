<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Quiz System</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="includes/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="includes/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="includes/lib/animate/animate.min.css" rel="stylesheet">
  <link href="includes/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="includes/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="includes/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="includes/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: NewBiz
    Theme URL: https://bootstrapmade.com/newbiz-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

 
  <section id="intro" class="clearfix">
    <div class="container">
    <div class="row">
   
    <div class="col-9">
      <div class="intro-img">
       <div class="header">
          <h2 class="text-light">Admin</h2>
        </div>
        <form method="post" action="adminlogin.php">
         <div class="form-group">
            <label  class="text-light">Username</label>
            <input type="text" name="username" class="form-control"   placeholder="Username" required>
         </div>
    
         <div class="form-group">
            <label  class="text-light">Password</label>
            <input type="password" name="password"  class="form-control" placeholder="Password" required>
         </div>
         
          <div class="form-group">
            <button type="submit" name="login" class="btn btn-primary">Login</button>
         </div>
         <p class="text-light">
           Back To Quiz Page?  <a href="index.php" class="text-danger">Sign in</a>
         </p>
       
          </form>
       
      </div> <!-- end of intro -->
      </div>
      <div class="col-3"></div>
   </div>
</div> <!-- end of container -->
  </section><!-- #intro -->